package com.shiguiwu.dobbu.mysql;

import com.shiguiwu.dubbo.api.DataSaveService;
import lombok.extern.slf4j.Slf4j;

/**
 * @description: mysql报错
 * @author: stone
 * @date: Created by 2023/1/12 11:16
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.dobbu.mysql
 */
@Slf4j
public class MysqlSaveService implements DataSaveService {
    @Override
    public void save(String data) {
        log.info("save in mysql data =====> {}" , data);

    }
}
