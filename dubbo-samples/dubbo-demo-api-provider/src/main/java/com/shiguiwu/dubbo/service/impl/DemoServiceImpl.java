package com.shiguiwu.dubbo.service.impl;

import com.shiguiwu.dubbo.DemoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.rpc.RpcContext;

import java.util.concurrent.CompletableFuture;

/**
 * @description:
 * @author: stone
 * @date: Created by 2023/1/16 11:26
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.dubbo.service.impl
 */
@Slf4j
public class DemoServiceImpl implements DemoService {


    @Override
    public String sayHello(String name) {
        log.info("Hello " + name + ", request from consumer: " + RpcContext.getServiceContext().getRemoteAddress());
        return "Hello " + name + ", response from provider: " + RpcContext.getServiceContext().getLocalAddress();
    }

    @Override
    public CompletableFuture<String> sayHelloAsync(String name) {
        return null;
    }

}
