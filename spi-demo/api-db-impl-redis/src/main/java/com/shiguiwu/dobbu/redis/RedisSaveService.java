package com.shiguiwu.dobbu.redis;

import com.shiguiwu.dubbo.api.DataSaveService;
import lombok.extern.slf4j.Slf4j;

/**
 * @description: redis
 * @author: stone
 * @date: Created by 2023/1/12 11:27
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.dobbu.redis
 */
@Slf4j
public class RedisSaveService implements DataSaveService {
    @Override
    public void save(String data) {
        log.info(" save in redis ===> {}" , data);

    }
}
