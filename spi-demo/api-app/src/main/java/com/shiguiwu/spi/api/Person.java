package com.shiguiwu.spi.api;

import org.apache.dubbo.common.extension.SPI;

/**
 * @description:
 * @author: stone
 * @date: Created by 2023/1/13 10:41
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.spi.api
 */
@SPI
public interface Person {

    Car getCar();
}
