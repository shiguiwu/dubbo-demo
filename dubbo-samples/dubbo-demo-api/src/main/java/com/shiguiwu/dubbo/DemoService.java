package com.shiguiwu.dubbo;

import java.util.concurrent.CompletableFuture;

/**
 * @description: 服务
 * @author: stone
 * @date: Created by 2023/1/16 11:24
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.dubbo
 */
public interface DemoService {

    /**
     * 同步处理的服务方法
     * @param name
     * @return
     */
    String sayHello(String name);

    /**
     * 用于异步处理的服务方法
     * @param name
     * @return
     */
    default CompletableFuture<String> sayHelloAsync(String name) {
        return CompletableFuture.completedFuture(sayHello(name));
    }

}
