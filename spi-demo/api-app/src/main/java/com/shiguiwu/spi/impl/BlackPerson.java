package com.shiguiwu.spi.impl;

import com.shiguiwu.spi.api.Person;
import com.shiguiwu.spi.api.Car;
import lombok.Setter;

/**
 * @description:
 * @author: stone
 * @date: Created by 2023/1/13 10:42
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.spi.impl
 */
@Setter
public class BlackPerson implements Person {

    private Car car;
    @Override
    public Car getCar() {
        return car;
    }
}
