package com.shiguiwu.dubbo.consumer;

import com.shiguiwu.dubbo.DemoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.config.ApplicationConfig;
import org.apache.dubbo.config.ProtocolConfig;
import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.apache.dubbo.config.bootstrap.DubboBootstrap;
import org.apache.dubbo.rpc.service.GenericService;

/**
 * @description: 消费者代码
 * @author: stone
 * @date: Created by 2023/8/5 18:28
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.dubbo.consumer
 */
@Slf4j
public class ConsumerApplication {

    public static void main(String[] args) {
        runWithBootstrap();
    }

    /**
     * 启动服务
     */
    private static void runWithBootstrap() {
        ReferenceConfig<DemoService> reference = new ReferenceConfig<>();
        reference.setInterface(DemoService.class);
        reference.setGeneric("true");
        reference.setProtocol("");

        DubboBootstrap bootstrap = DubboBootstrap.getInstance();
        ApplicationConfig applicationConfig = new ApplicationConfig("dubbo-demo-api-consumer");
        applicationConfig.setQosEnable(false);
        applicationConfig.setQosPort(-1);
        bootstrap.application(applicationConfig)
                .registry(new RegistryConfig("zookeeper://127.0.0.1:2181"))
                .protocol(new ProtocolConfig(CommonConstants.DUBBO, -1))
                .reference(reference)
                .start();

        DemoService demoService = bootstrap.getCache().get(reference);
        String message = demoService.sayHello("dubbo");
        log.info("调用服务结果 ===> {}",message);

        // generic invoke
        GenericService genericService = (GenericService) demoService;
        Object genericInvokeResult = genericService.$invoke("sayHello", new String[]{String.class.getName()},
                new Object[]{"dubbo generic invoke"});
        log.info("调用服务2结果 ===> {}",genericInvokeResult);
        bootstrap.await();
    }

}
