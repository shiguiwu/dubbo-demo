package com.shiguiwu.dubbo.api;

/**
 * @description: 数据保存接口
 * @author: stone
 * @date: Created by 2023/1/12 10:58
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.dubbo.api
 */
public interface DataSaveService {

    void save(String data);


}
