package com.shiguiwu.spring.provider;

import org.apache.dubbo.config.annotation.DubboService;

/**
 * @description: 服务
 * @author: stone
 * @date: Created by 2023/1/15 21:02
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.spring.provider
 */
@DubboService
public class ProviderService {
}
