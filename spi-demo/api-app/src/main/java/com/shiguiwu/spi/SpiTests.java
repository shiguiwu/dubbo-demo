package com.shiguiwu.spi;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.extension.ExtensionLoader;
import org.apache.dubbo.rpc.Protocol;

/**
 * @description:
 * @author: stone
 * @date: Created by 2023/1/13 9:20
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.spi
 */

@Slf4j
public class SpiTests {

    public static void main(String[] args) {
        ExtensionLoader<Protocol> extensionLoader = ExtensionLoader.getExtensionLoader(Protocol.class);
        Protocol http = extensionLoader.getExtension("dubbo");
        log.info("spi ===> {} ",http.toString());


    }
}
