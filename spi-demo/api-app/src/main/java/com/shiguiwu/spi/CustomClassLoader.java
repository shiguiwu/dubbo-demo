package com.shiguiwu.spi;

/**
 * @description:
 * @author: stone
 * @date: Created by 2023/6/6 14:22
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.spi
 */
public class CustomClassLoader extends ClassLoader {
    //static {
    //    try {
    //        new CustomClassLoader("api-db-impl-redis").loadClass("com.shiguiwu.dobbu.MutilClass");
    //    } catch (ClassNotFoundException e) {
    //        throw new RuntimeException(e);
    //    }
    //}

    private String moduleToLoadFirst;

    public CustomClassLoader(String moduleToLoadFirst) {
        this.moduleToLoadFirst = moduleToLoadFirst;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        try {
            // 先从指定模块加载类
            if ("api-db-impl-redis".equals(moduleToLoadFirst)) {
                try {
                    return findClass(name);
                } catch (ClassNotFoundException ignored) {
                    // 如果在指定模块中找不到类，则继续从其他模块加载
                }
            }

            // 从其他模块加载类
            return super.loadClass(name);
        } catch (ClassNotFoundException e) {
            // 如果在所有模块中都找不到类，则抛出 ClassNotFoundException
            throw new ClassNotFoundException("Class not found: " + name);
        }
    }


}
