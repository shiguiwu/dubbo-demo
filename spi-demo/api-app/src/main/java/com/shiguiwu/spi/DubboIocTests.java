package com.shiguiwu.spi;

import com.shiguiwu.spi.api.Person;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.URL;
import org.apache.dubbo.common.extension.ExtensionLoader;

/**
 * @description: /
 * @author: stone
 * @date: Created by 2023/1/13 10:49
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.spi
 */
@Slf4j
public class DubboIocTests {

    public static void main(String[] args) {
        ExtensionLoader<Person> extensionLoader = ExtensionLoader.getExtensionLoader(Person.class);
        Person person = extensionLoader.getExtension("black");  // BlackPerson
        URL url = new URL("x", "localhost", 8080);
        url.addParameter("car", "red");

        log.info(person.getCar().getCarName(url));
    }




}
