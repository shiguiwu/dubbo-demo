package com.shiguiwu.spring;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;

/**
 * @description: 主方法
 * @author: stone
 * @date: Created by 2023/1/15 20:21
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.spring
 */
@Slf4j
public class Application {

    public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ProviderConfiguration.class);
        context.start();

        int read = System.in.read();

        log.info("read ===> {}", read);

    }




    @Configuration
    @EnableDubbo(scanBasePackages = "com.shiguiwu.spring.provider")
    @PropertySource("classpath:/spring/dubbo-provider.properties")
    public static class ProviderConfiguration {

    }
}
