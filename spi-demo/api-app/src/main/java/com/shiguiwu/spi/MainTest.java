package com.shiguiwu.spi;

import com.shiguiwu.dubbo.api.DataSaveService;
import lombok.extern.slf4j.Slf4j;

import java.util.ServiceLoader;

/**
 * @description: 主测试类
 * @author: stone
 * @date: Created by 2023/1/12 12:21
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.spi
 */

@Slf4j
public class MainTest {



    public static void main(String[] args) {
        ServiceLoader<DataSaveService> load = ServiceLoader.load(DataSaveService.class);

        //拿到实现进行调用
        for (DataSaveService service : load) {
            service.save("你好....");
        }

    }
}
