package com.shiguiwu.spi.impl;

import com.shiguiwu.spi.api.Car;
import org.apache.dubbo.common.URL;

/**
 * @description: 黑车
 * @author: stone
 * @date: Created by 2023/1/13 11:21
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.spi.impl
 */
public class BlackCar implements Car {
    @Override
    public String getCarName(URL url) {
        return "黑车";
    }
}
