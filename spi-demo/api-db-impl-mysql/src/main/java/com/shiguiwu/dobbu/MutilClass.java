package com.shiguiwu.dobbu;

import lombok.extern.slf4j.Slf4j;

/**
 * @description: 多个类，生成同一个
 * @author: stone
 * @date: Created by 2023/6/6 14:10
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.dobbu
 */
@Slf4j
public class MutilClass {

    static {
        log.info("redis is class --------我是谁 {}", MutilClass.class.getName());
        log.info("redis iss class --------我是谁 {}", MutilClass.class.getName());
    }
}
