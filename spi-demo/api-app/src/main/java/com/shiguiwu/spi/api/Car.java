package com.shiguiwu.spi.api;

import lombok.Data;
import org.apache.dubbo.common.URL;
import org.apache.dubbo.common.extension.Adaptive;
import org.apache.dubbo.common.extension.SPI;

/**
 * @description:
 * @author: stone
 * @date: Created by 2023/1/13 10:42
 * @version: 1.0.0
 * @pakeage: com.shiguiwu.spi.model
 */
@SPI("red")
public interface Car {


    @Adaptive
    String getCarName(URL url);
}
